﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2021.1.3),
    on 4월 03, 2021, at 23:13
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""

from __future__ import absolute_import, division

from psychopy import locale_setup
from psychopy import prefs
from psychopy import sound, gui, visual, core, data, event, logging, clock, colors
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle, choice as randchoice
import os  # handy system and path functions
import sys  # to get file system encoding

from psychopy.hardware import keyboard



# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
os.chdir(_thisDir)

# Store info about the experiment session
psychopyVersion = '2021.1.3'
expName = 'exp'  # from the Builder filename that created this script
expInfo = {'participant': '', 'session': '001'}
dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
if dlg.OK == False:
    core.quit()  # user pressed cancel
expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName
expInfo['psychopyVersion'] = psychopyVersion

# Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
filename = _thisDir + os.sep + u'data/%s_%s_%s' % (expInfo['participant'], expName, expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath='C:\\exp\\exp.py',
    savePickle=True, saveWideText=True,
    dataFileName=filename)
# save a log file for detail verbose info
logFile = logging.LogFile(filename+'.log', level=logging.EXP)
logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

endExpNow = False  # flag for 'escape' or other condition => quit the exp
frameTolerance = 0.001  # how close to onset before 'same' frame

# Start Code - component code to be run after the window creation

# Setup the Window
win = visual.Window(
    size=[1536, 864], fullscr=True, screen=0, 
    winType='pyglet', allowGUI=False, allowStencil=False,
    monitor='testMonitor', color='white', colorSpace='rgb',
    blendMode='avg', useFBO=True, 
    units='height')
# store frame rate of monitor if we can measure it
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] != None:
    frameDur = 1.0 / round(expInfo['frameRate'])
else:
    frameDur = 1.0 / 60.0  # could not measure, so guess

# create a default keyboard (e.g. to check for escape)
defaultKeyboard = keyboard.Keyboard()

# Initialize components for Routine "welcome"
welcomeClock = core.Clock()
mouse_welcome = event.Mouse(win=win)
x, y = [None, None]
mouse_welcome.mouseClock = core.Clock()
image_welcome = visual.ImageStim(
    win=win,
    name='image_welcome', 
    image='welcome.JPG', mask=None,
    ori=0.0, pos=(0, 0), size=(1.7, 1),
    color=[1,1,1], colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=128.0, interpolate=True, depth=-1.0)

# Initialize components for Routine "inst1"
inst1Clock = core.Clock()
image_inst1 = visual.ImageStim(
    win=win,
    name='image_inst1', 
    image='instr1.JPG', mask=None,
    ori=0.0, pos=(0, 0), size=(1.7, 1),
    color=[1,1,1], colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=128.0, interpolate=True, depth=0.0)
mouse_inst1 = event.Mouse(win=win)
x, y = [None, None]
mouse_inst1.mouseClock = core.Clock()

# Initialize components for Routine "inst2"
inst2Clock = core.Clock()
image_inst2 = visual.ImageStim(
    win=win,
    name='image_inst2', 
    image='instr2.JPG', mask=None,
    ori=0.0, pos=(0, 0), size=(1.7, 1),
    color=[1,1,1], colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=128.0, interpolate=True, depth=0.0)
mouse_inst2 = event.Mouse(win=win)
x, y = [None, None]
mouse_inst2.mouseClock = core.Clock()

# Initialize components for Routine "inst3"
inst3Clock = core.Clock()
image_inst3 = visual.ImageStim(
    win=win,
    name='image_inst3', 
    image='instr3.JPG', mask=None,
    ori=0.0, pos=(0, 0), size=(1.7, 1),
    color=[1,1,1], colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=128.0, interpolate=True, depth=0.0)
mouse_inst3 = event.Mouse(win=win)
x, y = [None, None]
mouse_inst3.mouseClock = core.Clock()

# Initialize components for Routine "fixation"
fixationClock = core.Clock()
text_fixation = visual.TextStim(win=win, name='text_fixation',
    text='+',
    font='Open Sans',
    pos=(0, 0), height=0.2, wrapWidth=None, ori=0.0, 
    color='black', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "desk"
deskClock = core.Clock()
text_desk = visual.TextStim(win=win, name='text_desk',
    text='책상',
    font='Malgun Gothic',
    pos=(0, 0), height=0.15, wrapWidth=None, ori=0.0, 
    color='black', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
mouse_desk = event.Mouse(win=win)
x, y = [None, None]
mouse_desk.mouseClock = core.Clock()

# Initialize components for Routine "response"
responseClock = core.Clock()
text_response = visual.TextStim(win=win, name='text_response',
    text='60초 이내에 구체적인 기억을 입력해주세요. \n(완료되셨다면 하단의 선을 클릭해주세요. 60초가 지나면 자동으로 다음 화면으로 넘어갑니다)\n',
    font='Malgun Gothic',
    pos=(0, 0.25), height=0.03, wrapWidth=None, ori=0.0, 
    color='black', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
textbox = visual.TextBox2(
     win, text=' ', font='Malgun Gothic',
     pos=(0, -.1),     letterHeight=0.03,
     size=(1.3,0.5), borderWidth=2.0,
     color='black', colorSpace='rgb',
     opacity=None,
     bold=False, italic=False,
     lineSpacing=1.0,
     padding=None,
     anchor='center',
     fillColor=None, borderColor='darkslategray',
     flipHoriz=False, flipVert=False,
     editable=True,
     name='textbox',
     autoLog=True,
)
text_9 = visual.TextStim(win=win, name='text_9',
    text='',
    font='Malgun Gothic',
    pos=(-0.6, 0.3), height=0.05, wrapWidth=None, ori=0.0, 
    color='black', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=-2.0);
# Set experiment start values for variable component var1
var1 = [ ]
var1Container = []
slider = visual.Slider(win=win, name='slider',
    size=(1.0, 0.1), pos=(0, -0.4), units=None,
    labels=None, ticks=(1, 2), granularity=0.0,
    style='rating', styleTweaks=(), opacity=None,
    color='LightGray', fillColor='Red', borderColor='black', colorSpace='rgb',
    font='Open Sans', labelHeight=0.05,
    flip=False, depth=-4, readOnly=False)

# Initialize components for Routine "askscreen"
askscreenClock = core.Clock()
image_ask = visual.ImageStim(
    win=win,
    name='image_ask', 
    image='ask.JPG', mask=None,
    ori=0.0, pos=(0, 0), size=(1.7, 1),
    color=[1,1,1], colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=128.0, interpolate=True, depth=0.0)
mouse_ask = event.Mouse(win=win)
x, y = [None, None]
mouse_ask.mouseClock = core.Clock()

# Initialize components for Routine "fixation"
fixationClock = core.Clock()
text_fixation = visual.TextStim(win=win, name='text_fixation',
    text='+',
    font='Open Sans',
    pos=(0, 0), height=0.2, wrapWidth=None, ori=0.0, 
    color='black', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "stimuli"
stimuliClock = core.Clock()
text_stimuli = visual.TextStim(win=win, name='text_stimuli',
    text='',
    font='Malgun Gothic',
    pos=(0, 0), height=0.15, wrapWidth=None, ori=0.0, 
    color='black', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
mouse_stimuli = event.Mouse(win=win)
x, y = [None, None]
mouse_stimuli.mouseClock = core.Clock()

# Initialize components for Routine "response"
responseClock = core.Clock()
text_response = visual.TextStim(win=win, name='text_response',
    text='60초 이내에 구체적인 기억을 입력해주세요. \n(완료되셨다면 하단의 선을 클릭해주세요. 60초가 지나면 자동으로 다음 화면으로 넘어갑니다)\n',
    font='Malgun Gothic',
    pos=(0, 0.25), height=0.03, wrapWidth=None, ori=0.0, 
    color='black', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
textbox = visual.TextBox2(
     win, text=' ', font='Malgun Gothic',
     pos=(0, -.1),     letterHeight=0.03,
     size=(1.3,0.5), borderWidth=2.0,
     color='black', colorSpace='rgb',
     opacity=None,
     bold=False, italic=False,
     lineSpacing=1.0,
     padding=None,
     anchor='center',
     fillColor=None, borderColor='darkslategray',
     flipHoriz=False, flipVert=False,
     editable=True,
     name='textbox',
     autoLog=True,
)
text_9 = visual.TextStim(win=win, name='text_9',
    text='',
    font='Malgun Gothic',
    pos=(-0.6, 0.3), height=0.05, wrapWidth=None, ori=0.0, 
    color='black', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=-2.0);
# Set experiment start values for variable component var1
var1 = [ ]
var1Container = []
slider = visual.Slider(win=win, name='slider',
    size=(1.0, 0.1), pos=(0, -0.4), units=None,
    labels=None, ticks=(1, 2), granularity=0.0,
    style='rating', styleTweaks=(), opacity=None,
    color='LightGray', fillColor='Red', borderColor='black', colorSpace='rgb',
    font='Open Sans', labelHeight=0.05,
    flip=False, depth=-4, readOnly=False)

# Create some handy timers
globalClock = core.Clock()  # to track the time since experiment started
routineTimer = core.CountdownTimer()  # to track time remaining of each (non-slip) routine 

# ------Prepare to start Routine "welcome"-------
continueRoutine = True
# update component parameters for each repeat
# setup some python lists for storing info about the mouse_welcome
gotValidClick = False  # until a click is received
# keep track of which components have finished
welcomeComponents = [mouse_welcome, image_welcome]
for thisComponent in welcomeComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
welcomeClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "welcome"-------
while continueRoutine:
    # get current time
    t = welcomeClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=welcomeClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    # *mouse_welcome* updates
    if mouse_welcome.status == NOT_STARTED and t >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        mouse_welcome.frameNStart = frameN  # exact frame index
        mouse_welcome.tStart = t  # local t and not account for scr refresh
        mouse_welcome.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(mouse_welcome, 'tStartRefresh')  # time at next scr refresh
        mouse_welcome.status = STARTED
        mouse_welcome.mouseClock.reset()
        prevButtonState = mouse_welcome.getPressed()  # if button is down already this ISN'T a new click
    if mouse_welcome.status == STARTED:  # only update if started and not finished!
        buttons = mouse_welcome.getPressed()
        if buttons != prevButtonState:  # button state changed?
            prevButtonState = buttons
            if sum(buttons) > 0:  # state changed to a new click
                continueRoutine = False    
    # *image_welcome* updates
    if image_welcome.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        image_welcome.frameNStart = frameN  # exact frame index
        image_welcome.tStart = t  # local t and not account for scr refresh
        image_welcome.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(image_welcome, 'tStartRefresh')  # time at next scr refresh
        image_welcome.setAutoDraw(True)
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in welcomeComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "welcome"-------
for thisComponent in welcomeComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# store data for thisExp (ExperimentHandler)
thisExp.addData('mouse_welcome.started', mouse_welcome.tStart)
thisExp.addData('mouse_welcome.stopped', mouse_welcome.tStop)
thisExp.nextEntry()
# the Routine "welcome" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "inst1"-------
continueRoutine = True
# update component parameters for each repeat
# setup some python lists for storing info about the mouse_inst1
gotValidClick = False  # until a click is received
# keep track of which components have finished
inst1Components = [image_inst1, mouse_inst1]
for thisComponent in inst1Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
inst1Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "inst1"-------
while continueRoutine:
    # get current time
    t = inst1Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=inst1Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *image_inst1* updates
    if image_inst1.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        image_inst1.frameNStart = frameN  # exact frame index
        image_inst1.tStart = t  # local t and not account for scr refresh
        image_inst1.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(image_inst1, 'tStartRefresh')  # time at next scr refresh
        image_inst1.setAutoDraw(True)
    # *mouse_inst1* updates
    if mouse_inst1.status == NOT_STARTED and t >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        mouse_inst1.frameNStart = frameN  # exact frame index
        mouse_inst1.tStart = t  # local t and not account for scr refresh
        mouse_inst1.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(mouse_inst1, 'tStartRefresh')  # time at next scr refresh
        mouse_inst1.status = STARTED
        mouse_inst1.mouseClock.reset()
        prevButtonState = mouse_inst1.getPressed()  # if button is down already this ISN'T a new click
    if mouse_inst1.status == STARTED:  # only update if started and not finished!
        buttons = mouse_inst1.getPressed()
        if buttons != prevButtonState:  # button state changed?
            prevButtonState = buttons
            if sum(buttons) > 0:  # state changed to a new click
                # abort routine on response
                continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in inst1Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "inst1"-------
for thisComponent in inst1Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('image_inst1.started', image_inst1.tStartRefresh)
thisExp.addData('image_inst1.stopped', image_inst1.tStopRefresh)
# store data for thisExp (ExperimentHandler)
x, y = mouse_inst1.getPos()
buttons = mouse_inst1.getPressed()
thisExp.addData('mouse_inst1.x', x)
thisExp.addData('mouse_inst1.y', y)
thisExp.addData('mouse_inst1.leftButton', buttons[0])
thisExp.addData('mouse_inst1.midButton', buttons[1])
thisExp.addData('mouse_inst1.rightButton', buttons[2])
thisExp.nextEntry()
# the Routine "inst1" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "inst2"-------
continueRoutine = True
# update component parameters for each repeat
# setup some python lists for storing info about the mouse_inst2
gotValidClick = False  # until a click is received
# keep track of which components have finished
inst2Components = [image_inst2, mouse_inst2]
for thisComponent in inst2Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
inst2Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "inst2"-------
while continueRoutine:
    # get current time
    t = inst2Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=inst2Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *image_inst2* updates
    if image_inst2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        image_inst2.frameNStart = frameN  # exact frame index
        image_inst2.tStart = t  # local t and not account for scr refresh
        image_inst2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(image_inst2, 'tStartRefresh')  # time at next scr refresh
        image_inst2.setAutoDraw(True)
    # *mouse_inst2* updates
    if mouse_inst2.status == NOT_STARTED and t >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        mouse_inst2.frameNStart = frameN  # exact frame index
        mouse_inst2.tStart = t  # local t and not account for scr refresh
        mouse_inst2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(mouse_inst2, 'tStartRefresh')  # time at next scr refresh
        mouse_inst2.status = STARTED
        mouse_inst2.mouseClock.reset()
        prevButtonState = mouse_inst2.getPressed()  # if button is down already this ISN'T a new click
    if mouse_inst2.status == STARTED:  # only update if started and not finished!
        buttons = mouse_inst2.getPressed()
        if buttons != prevButtonState:  # button state changed?
            prevButtonState = buttons
            if sum(buttons) > 0:  # state changed to a new click
                # abort routine on response
                continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in inst2Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "inst2"-------
for thisComponent in inst2Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# store data for thisExp (ExperimentHandler)
x, y = mouse_inst2.getPos()
buttons = mouse_inst2.getPressed()
thisExp.addData('mouse_inst2.x', x)
thisExp.addData('mouse_inst2.y', y)
thisExp.addData('mouse_inst2.leftButton', buttons[0])
thisExp.addData('mouse_inst2.midButton', buttons[1])
thisExp.addData('mouse_inst2.rightButton', buttons[2])
thisExp.addData('mouse_inst2.started', mouse_inst2.tStart)
thisExp.addData('mouse_inst2.stopped', mouse_inst2.tStop)
thisExp.nextEntry()
# the Routine "inst2" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "inst3"-------
continueRoutine = True
# update component parameters for each repeat
# setup some python lists for storing info about the mouse_inst3
gotValidClick = False  # until a click is received
# keep track of which components have finished
inst3Components = [image_inst3, mouse_inst3]
for thisComponent in inst3Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
inst3Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "inst3"-------
while continueRoutine:
    # get current time
    t = inst3Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=inst3Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *image_inst3* updates
    if image_inst3.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        image_inst3.frameNStart = frameN  # exact frame index
        image_inst3.tStart = t  # local t and not account for scr refresh
        image_inst3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(image_inst3, 'tStartRefresh')  # time at next scr refresh
        image_inst3.setAutoDraw(True)
    # *mouse_inst3* updates
    if mouse_inst3.status == NOT_STARTED and t >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        mouse_inst3.frameNStart = frameN  # exact frame index
        mouse_inst3.tStart = t  # local t and not account for scr refresh
        mouse_inst3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(mouse_inst3, 'tStartRefresh')  # time at next scr refresh
        mouse_inst3.status = STARTED
        mouse_inst3.mouseClock.reset()
        prevButtonState = mouse_inst3.getPressed()  # if button is down already this ISN'T a new click
    if mouse_inst3.status == STARTED:  # only update if started and not finished!
        buttons = mouse_inst3.getPressed()
        if buttons != prevButtonState:  # button state changed?
            prevButtonState = buttons
            if sum(buttons) > 0:  # state changed to a new click
                # abort routine on response
                continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in inst3Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "inst3"-------
for thisComponent in inst3Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# store data for thisExp (ExperimentHandler)
x, y = mouse_inst3.getPos()
buttons = mouse_inst3.getPressed()
thisExp.addData('mouse_inst3.x', x)
thisExp.addData('mouse_inst3.y', y)
thisExp.addData('mouse_inst3.leftButton', buttons[0])
thisExp.addData('mouse_inst3.midButton', buttons[1])
thisExp.addData('mouse_inst3.rightButton', buttons[2])
thisExp.addData('mouse_inst3.started', mouse_inst3.tStart)
thisExp.addData('mouse_inst3.stopped', mouse_inst3.tStop)
thisExp.nextEntry()
# the Routine "inst3" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "fixation"-------
continueRoutine = True
routineTimer.add(1.000000)
# update component parameters for each repeat
# keep track of which components have finished
fixationComponents = [text_fixation]
for thisComponent in fixationComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
fixationClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "fixation"-------
while continueRoutine and routineTimer.getTime() > 0:
    # get current time
    t = fixationClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=fixationClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *text_fixation* updates
    if text_fixation.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        text_fixation.frameNStart = frameN  # exact frame index
        text_fixation.tStart = t  # local t and not account for scr refresh
        text_fixation.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(text_fixation, 'tStartRefresh')  # time at next scr refresh
        text_fixation.setAutoDraw(True)
    if text_fixation.status == STARTED:
        # is it time to stop? (based on global clock, using actual start)
        if tThisFlipGlobal > text_fixation.tStartRefresh + 1.0-frameTolerance:
            # keep track of stop time/frame for later
            text_fixation.tStop = t  # not accounting for scr refresh
            text_fixation.frameNStop = frameN  # exact frame index
            win.timeOnFlip(text_fixation, 'tStopRefresh')  # time at next scr refresh
            text_fixation.setAutoDraw(False)
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in fixationComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "fixation"-------
for thisComponent in fixationComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('text_fixation.started', text_fixation.tStartRefresh)
thisExp.addData('text_fixation.stopped', text_fixation.tStopRefresh)

# ------Prepare to start Routine "desk"-------
continueRoutine = True
# update component parameters for each repeat
# setup some python lists for storing info about the mouse_desk
gotValidClick = False  # until a click is received
# keep track of which components have finished
deskComponents = [text_desk, mouse_desk]
for thisComponent in deskComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
deskClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "desk"-------
while continueRoutine:
    # get current time
    t = deskClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=deskClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *text_desk* updates
    if text_desk.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        text_desk.frameNStart = frameN  # exact frame index
        text_desk.tStart = t  # local t and not account for scr refresh
        text_desk.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(text_desk, 'tStartRefresh')  # time at next scr refresh
        text_desk.setAutoDraw(True)
    # *mouse_desk* updates
    if mouse_desk.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        mouse_desk.frameNStart = frameN  # exact frame index
        mouse_desk.tStart = t  # local t and not account for scr refresh
        mouse_desk.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(mouse_desk, 'tStartRefresh')  # time at next scr refresh
        mouse_desk.status = STARTED
        mouse_desk.mouseClock.reset()
        prevButtonState = mouse_desk.getPressed()  # if button is down already this ISN'T a new click
    if mouse_desk.status == STARTED:  # only update if started and not finished!
        buttons = mouse_desk.getPressed()
        if buttons != prevButtonState:  # button state changed?
            prevButtonState = buttons
            if sum(buttons) > 0:  # state changed to a new click
                # abort routine on response
                continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in deskComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "desk"-------
for thisComponent in deskComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('text_desk.started', text_desk.tStartRefresh)
thisExp.addData('text_desk.stopped', text_desk.tStopRefresh)
# store data for thisExp (ExperimentHandler)
x, y = mouse_desk.getPos()
buttons = mouse_desk.getPressed()
thisExp.addData('mouse_desk.x', x)
thisExp.addData('mouse_desk.y', y)
thisExp.addData('mouse_desk.leftButton', buttons[0])
thisExp.addData('mouse_desk.midButton', buttons[1])
thisExp.addData('mouse_desk.rightButton', buttons[2])
thisExp.addData('mouse_desk.started', mouse_desk.tStartRefresh)
thisExp.addData('mouse_desk.stopped', mouse_desk.tStopRefresh)
thisExp.nextEntry()
# the Routine "desk" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "response"-------
continueRoutine = True
# update component parameters for each repeat
slider.reset()
# keep track of which components have finished
responseComponents = [text_response, textbox, text_9, slider]
for thisComponent in responseComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
responseClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "response"-------
while continueRoutine:
    # get current time
    t = responseClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=responseClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *text_response* updates
    if text_response.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        text_response.frameNStart = frameN  # exact frame index
        text_response.tStart = t  # local t and not account for scr refresh
        text_response.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(text_response, 'tStartRefresh')  # time at next scr refresh
        text_response.setAutoDraw(True)
    if text_response.status == STARTED:
        # is it time to stop? (based on global clock, using actual start)
        if tThisFlipGlobal > text_response.tStartRefresh + 60.0-frameTolerance:
            # keep track of stop time/frame for later
            text_response.tStop = t  # not accounting for scr refresh
            text_response.frameNStop = frameN  # exact frame index
            win.timeOnFlip(text_response, 'tStopRefresh')  # time at next scr refresh
            text_response.setAutoDraw(False)
    
    # *textbox* updates
    if textbox.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        textbox.frameNStart = frameN  # exact frame index
        textbox.tStart = t  # local t and not account for scr refresh
        textbox.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(textbox, 'tStartRefresh')  # time at next scr refresh
        textbox.setAutoDraw(True)
    if textbox.status == STARTED:
        # is it time to stop? (based on global clock, using actual start)
        if tThisFlipGlobal > textbox.tStartRefresh + 60.0-frameTolerance:
            # keep track of stop time/frame for later
            textbox.tStop = t  # not accounting for scr refresh
            textbox.frameNStop = frameN  # exact frame index
            win.timeOnFlip(textbox, 'tStopRefresh')  # time at next scr refresh
            textbox.setAutoDraw(False)
    
    # *text_9* updates
    if text_9.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        text_9.frameNStart = frameN  # exact frame index
        text_9.tStart = t  # local t and not account for scr refresh
        text_9.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(text_9, 'tStartRefresh')  # time at next scr refresh
        text_9.setAutoDraw(True)
    if text_9.status == STARTED:
        # is it time to stop? (based on global clock, using actual start)
        if tThisFlipGlobal > text_9.tStartRefresh + 60.0-frameTolerance:
            # keep track of stop time/frame for later
            text_9.tStop = t  # not accounting for scr refresh
            text_9.frameNStop = frameN  # exact frame index
            win.timeOnFlip(text_9, 'tStopRefresh')  # time at next scr refresh
            text_9.setAutoDraw(False)
    if text_9.status == STARTED:  # only update if drawing
        text_9.setText(round(60.0 - t))
    
    # *slider* updates
    if slider.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        slider.frameNStart = frameN  # exact frame index
        slider.tStart = t  # local t and not account for scr refresh
        slider.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(slider, 'tStartRefresh')  # time at next scr refresh
        slider.setAutoDraw(True)
    
    # Check slider for response to end routine
    if slider.getRating() is not None and slider.status == STARTED:
        continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in responseComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "response"-------
for thisComponent in responseComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('textbox.text',textbox.text)
textbox.reset()
thisExp.addData('textbox.started', textbox.tStartRefresh)
thisExp.addData('textbox.stopped', textbox.tStopRefresh)
thisExp.addData('text_9.started', text_9.tStartRefresh)
thisExp.addData('text_9.stopped', text_9.tStopRefresh)
thisExp.addData('slider.response', slider.getRating())
thisExp.addData('slider.rt', slider.getRT())
thisExp.addData('slider.started', slider.tStartRefresh)
thisExp.addData('slider.stopped', slider.tStopRefresh)
# the Routine "response" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "askscreen"-------
continueRoutine = True
# update component parameters for each repeat
# setup some python lists for storing info about the mouse_ask
gotValidClick = False  # until a click is received
# keep track of which components have finished
askscreenComponents = [image_ask, mouse_ask]
for thisComponent in askscreenComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
askscreenClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "askscreen"-------
while continueRoutine:
    # get current time
    t = askscreenClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=askscreenClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *image_ask* updates
    if image_ask.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        image_ask.frameNStart = frameN  # exact frame index
        image_ask.tStart = t  # local t and not account for scr refresh
        image_ask.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(image_ask, 'tStartRefresh')  # time at next scr refresh
        image_ask.setAutoDraw(True)
    # *mouse_ask* updates
    if mouse_ask.status == NOT_STARTED and t >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        mouse_ask.frameNStart = frameN  # exact frame index
        mouse_ask.tStart = t  # local t and not account for scr refresh
        mouse_ask.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(mouse_ask, 'tStartRefresh')  # time at next scr refresh
        mouse_ask.status = STARTED
        mouse_ask.mouseClock.reset()
        prevButtonState = mouse_ask.getPressed()  # if button is down already this ISN'T a new click
    if mouse_ask.status == STARTED:  # only update if started and not finished!
        buttons = mouse_ask.getPressed()
        if buttons != prevButtonState:  # button state changed?
            prevButtonState = buttons
            if sum(buttons) > 0:  # state changed to a new click
                # abort routine on response
                continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in askscreenComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "askscreen"-------
for thisComponent in askscreenComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# store data for thisExp (ExperimentHandler)
x, y = mouse_ask.getPos()
buttons = mouse_ask.getPressed()
thisExp.addData('mouse_ask.x', x)
thisExp.addData('mouse_ask.y', y)
thisExp.addData('mouse_ask.leftButton', buttons[0])
thisExp.addData('mouse_ask.midButton', buttons[1])
thisExp.addData('mouse_ask.rightButton', buttons[2])
thisExp.addData('mouse_ask.started', mouse_ask.tStart)
thisExp.addData('mouse_ask.stopped', mouse_ask.tStop)
thisExp.nextEntry()
# the Routine "askscreen" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
trials = data.TrialHandler(nReps=1.0, method='random', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('worditem.xlsx'),
    seed=None, name='trials')
thisExp.addLoop(trials)  # add the loop to the experiment
thisTrial = trials.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
if thisTrial != None:
    for paramName in thisTrial:
        exec('{} = thisTrial[paramName]'.format(paramName))

for thisTrial in trials:
    currentLoop = trials
    # abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
    if thisTrial != None:
        for paramName in thisTrial:
            exec('{} = thisTrial[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "fixation"-------
    continueRoutine = True
    routineTimer.add(1.000000)
    # update component parameters for each repeat
    # keep track of which components have finished
    fixationComponents = [text_fixation]
    for thisComponent in fixationComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    fixationClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "fixation"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = fixationClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=fixationClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *text_fixation* updates
        if text_fixation.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            text_fixation.frameNStart = frameN  # exact frame index
            text_fixation.tStart = t  # local t and not account for scr refresh
            text_fixation.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text_fixation, 'tStartRefresh')  # time at next scr refresh
            text_fixation.setAutoDraw(True)
        if text_fixation.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > text_fixation.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                text_fixation.tStop = t  # not accounting for scr refresh
                text_fixation.frameNStop = frameN  # exact frame index
                win.timeOnFlip(text_fixation, 'tStopRefresh')  # time at next scr refresh
                text_fixation.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in fixationComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "fixation"-------
    for thisComponent in fixationComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    trials.addData('text_fixation.started', text_fixation.tStartRefresh)
    trials.addData('text_fixation.stopped', text_fixation.tStopRefresh)
    
    # ------Prepare to start Routine "stimuli"-------
    continueRoutine = True
    # update component parameters for each repeat
    text_stimuli.setText(worditem)
    # setup some python lists for storing info about the mouse_stimuli
    gotValidClick = False  # until a click is received
    # keep track of which components have finished
    stimuliComponents = [text_stimuli, mouse_stimuli]
    for thisComponent in stimuliComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    stimuliClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "stimuli"-------
    while continueRoutine:
        # get current time
        t = stimuliClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=stimuliClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *text_stimuli* updates
        if text_stimuli.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            text_stimuli.frameNStart = frameN  # exact frame index
            text_stimuli.tStart = t  # local t and not account for scr refresh
            text_stimuli.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text_stimuli, 'tStartRefresh')  # time at next scr refresh
            text_stimuli.setAutoDraw(True)
        # *mouse_stimuli* updates
        if mouse_stimuli.status == NOT_STARTED and t >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            mouse_stimuli.frameNStart = frameN  # exact frame index
            mouse_stimuli.tStart = t  # local t and not account for scr refresh
            mouse_stimuli.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(mouse_stimuli, 'tStartRefresh')  # time at next scr refresh
            mouse_stimuli.status = STARTED
            mouse_stimuli.mouseClock.reset()
            prevButtonState = mouse_stimuli.getPressed()  # if button is down already this ISN'T a new click
        if mouse_stimuli.status == STARTED:  # only update if started and not finished!
            buttons = mouse_stimuli.getPressed()
            if buttons != prevButtonState:  # button state changed?
                prevButtonState = buttons
                if sum(buttons) > 0:  # state changed to a new click
                    # abort routine on response
                    continueRoutine = False
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in stimuliComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "stimuli"-------
    for thisComponent in stimuliComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    trials.addData('text_stimuli.started', text_stimuli.tStartRefresh)
    trials.addData('text_stimuli.stopped', text_stimuli.tStopRefresh)
    # store data for trials (TrialHandler)
    x, y = mouse_stimuli.getPos()
    buttons = mouse_stimuli.getPressed()
    trials.addData('mouse_stimuli.x', x)
    trials.addData('mouse_stimuli.y', y)
    trials.addData('mouse_stimuli.leftButton', buttons[0])
    trials.addData('mouse_stimuli.midButton', buttons[1])
    trials.addData('mouse_stimuli.rightButton', buttons[2])
    trials.addData('mouse_stimuli.started', mouse_stimuli.tStart)
    trials.addData('mouse_stimuli.stopped', mouse_stimuli.tStop)
    # the Routine "stimuli" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # ------Prepare to start Routine "response"-------
    continueRoutine = True
    # update component parameters for each repeat
    slider.reset()
    # keep track of which components have finished
    responseComponents = [text_response, textbox, text_9, slider]
    for thisComponent in responseComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    responseClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "response"-------
    while continueRoutine:
        # get current time
        t = responseClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=responseClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *text_response* updates
        if text_response.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            text_response.frameNStart = frameN  # exact frame index
            text_response.tStart = t  # local t and not account for scr refresh
            text_response.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text_response, 'tStartRefresh')  # time at next scr refresh
            text_response.setAutoDraw(True)
        if text_response.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > text_response.tStartRefresh + 60.0-frameTolerance:
                # keep track of stop time/frame for later
                text_response.tStop = t  # not accounting for scr refresh
                text_response.frameNStop = frameN  # exact frame index
                win.timeOnFlip(text_response, 'tStopRefresh')  # time at next scr refresh
                text_response.setAutoDraw(False)
        
        # *textbox* updates
        if textbox.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            textbox.frameNStart = frameN  # exact frame index
            textbox.tStart = t  # local t and not account for scr refresh
            textbox.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(textbox, 'tStartRefresh')  # time at next scr refresh
            textbox.setAutoDraw(True)
        if textbox.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > textbox.tStartRefresh + 60.0-frameTolerance:
                # keep track of stop time/frame for later
                textbox.tStop = t  # not accounting for scr refresh
                textbox.frameNStop = frameN  # exact frame index
                win.timeOnFlip(textbox, 'tStopRefresh')  # time at next scr refresh
                textbox.setAutoDraw(False)
        
        # *text_9* updates
        if text_9.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            text_9.frameNStart = frameN  # exact frame index
            text_9.tStart = t  # local t and not account for scr refresh
            text_9.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text_9, 'tStartRefresh')  # time at next scr refresh
            text_9.setAutoDraw(True)
        if text_9.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > text_9.tStartRefresh + 60.0-frameTolerance:
                # keep track of stop time/frame for later
                text_9.tStop = t  # not accounting for scr refresh
                text_9.frameNStop = frameN  # exact frame index
                win.timeOnFlip(text_9, 'tStopRefresh')  # time at next scr refresh
                text_9.setAutoDraw(False)
        if text_9.status == STARTED:  # only update if drawing
            text_9.setText(round(60.0 - t))
        
        # *slider* updates
        if slider.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            slider.frameNStart = frameN  # exact frame index
            slider.tStart = t  # local t and not account for scr refresh
            slider.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(slider, 'tStartRefresh')  # time at next scr refresh
            slider.setAutoDraw(True)
        
        # Check slider for response to end routine
        if slider.getRating() is not None and slider.status == STARTED:
            continueRoutine = False
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in responseComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "response"-------
    for thisComponent in responseComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    trials.addData('textbox.text',textbox.text)
    textbox.reset()
    trials.addData('textbox.started', textbox.tStartRefresh)
    trials.addData('textbox.stopped', textbox.tStopRefresh)
    trials.addData('text_9.started', text_9.tStartRefresh)
    trials.addData('text_9.stopped', text_9.tStopRefresh)
    trials.addData('slider.response', slider.getRating())
    trials.addData('slider.rt', slider.getRT())
    trials.addData('slider.started', slider.tStartRefresh)
    trials.addData('slider.stopped', slider.tStopRefresh)
    # the Routine "response" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    thisExp.nextEntry()
    
# completed 1.0 repeats of 'trials'


# Flip one final time so any remaining win.callOnFlip() 
# and win.timeOnFlip() tasks get executed before quitting
win.flip()

# these shouldn't be strictly necessary (should auto-save)
thisExp.saveAsWideText(filename+'.csv', delim='auto')
thisExp.saveAsPickle(filename)
logging.flush()
# make sure everything is closed down
thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
