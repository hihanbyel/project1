﻿/************ 
 * Exp Test *
 ************/

// init psychoJS:
const psychoJS = new PsychoJS({
  debug: true
});

// open window:
psychoJS.openWindow({
  fullscr: true,
  color: new util.Color('white'),
  units: 'height',
  waitBlanking: true
});

// store info about the experiment session:
let expName = 'exp';  // from the Builder filename that created this script
let expInfo = {'participant': '', 'session': '001'};

// Start code blocks for 'Before Experiment'
// schedule the experiment:
psychoJS.schedule(psychoJS.gui.DlgFromDict({
  dictionary: expInfo,
  title: expName
}));

const flowScheduler = new Scheduler(psychoJS);
const dialogCancelScheduler = new Scheduler(psychoJS);
psychoJS.scheduleCondition(function() { return (psychoJS.gui.dialogComponent.button === 'OK'); }, flowScheduler, dialogCancelScheduler);

// flowScheduler gets run if the participants presses OK
flowScheduler.add(updateInfo); // add timeStamp
flowScheduler.add(experimentInit);
flowScheduler.add(welcomeRoutineBegin());
flowScheduler.add(welcomeRoutineEachFrame());
flowScheduler.add(welcomeRoutineEnd());
flowScheduler.add(inst1RoutineBegin());
flowScheduler.add(inst1RoutineEachFrame());
flowScheduler.add(inst1RoutineEnd());
flowScheduler.add(inst2RoutineBegin());
flowScheduler.add(inst2RoutineEachFrame());
flowScheduler.add(inst2RoutineEnd());
flowScheduler.add(inst3RoutineBegin());
flowScheduler.add(inst3RoutineEachFrame());
flowScheduler.add(inst3RoutineEnd());
flowScheduler.add(fixationRoutineBegin());
flowScheduler.add(fixationRoutineEachFrame());
flowScheduler.add(fixationRoutineEnd());
flowScheduler.add(deskRoutineBegin());
flowScheduler.add(deskRoutineEachFrame());
flowScheduler.add(deskRoutineEnd());
flowScheduler.add(responseRoutineBegin());
flowScheduler.add(responseRoutineEachFrame());
flowScheduler.add(responseRoutineEnd());
flowScheduler.add(askscreenRoutineBegin());
flowScheduler.add(askscreenRoutineEachFrame());
flowScheduler.add(askscreenRoutineEnd());
const trialsLoopScheduler = new Scheduler(psychoJS);
flowScheduler.add(trialsLoopBegin, trialsLoopScheduler);
flowScheduler.add(trialsLoopScheduler);
flowScheduler.add(trialsLoopEnd);
flowScheduler.add(quitPsychoJS, '', true);

// quit if user presses Cancel in dialog box:
dialogCancelScheduler.add(quitPsychoJS, '', false);

psychoJS.start({
  expName: expName,
  expInfo: expInfo,
  resources: [
    {'name': 'instr1.JPG', 'path': 'instr1.JPG'},
    {'name': 'welcome.JPG', 'path': 'welcome.JPG'},
    {'name': 'worditem.xlsx', 'path': 'worditem.xlsx'},
    {'name': 'ask.JPG', 'path': 'ask.JPG'},
    {'name': 'instr2.JPG', 'path': 'instr2.JPG'},
    {'name': 'instr3.JPG', 'path': 'instr3.JPG'}
  ]
});

psychoJS.experimentLogger.setLevel(core.Logger.ServerLevel.EXP);


var frameDur;
function updateInfo() {
  expInfo['date'] = util.MonotonicClock.getDateStr();  // add a simple timestamp
  expInfo['expName'] = expName;
  expInfo['psychopyVersion'] = '2021.1.3';
  expInfo['OS'] = window.navigator.platform;

  // store frame rate of monitor if we can measure it successfully
  expInfo['frameRate'] = psychoJS.window.getActualFrameRate();
  if (typeof expInfo['frameRate'] !== 'undefined')
    frameDur = 1.0 / Math.round(expInfo['frameRate']);
  else
    frameDur = 1.0 / 60.0; // couldn't get a reliable measure so guess

  // add info from the URL:
  util.addInfoFromUrl(expInfo);
  
  return Scheduler.Event.NEXT;
}


var welcomeClock;
var mouse_welcome;
var image_welcome;
var inst1Clock;
var image_inst1;
var mouse_inst1;
var inst2Clock;
var image_inst2;
var mouse_inst2;
var inst3Clock;
var image_inst3;
var mouse_inst3;
var fixationClock;
var text_fixation;
var deskClock;
var text_desk;
var mouse_desk;
var responseClock;
var text_response;
var textbox;
var text_9;
var slider;
var askscreenClock;
var image_ask;
var mouse_ask;
var stimuliClock;
var text_stimuli;
var mouse_stimuli;
var globalClock;
var routineTimer;
function experimentInit() {
  // Initialize components for Routine "welcome"
  welcomeClock = new util.Clock();
  mouse_welcome = new core.Mouse({
    win: psychoJS.window,
  });
  mouse_welcome.mouseClock = new util.Clock();
  image_welcome = new visual.ImageStim({
    win : psychoJS.window,
    name : 'image_welcome', units : undefined, 
    image : 'welcome.JPG', mask : undefined,
    ori : 0.0, pos : [0, 0], size : [1.7, 1],
    color : new util.Color([1, 1, 1]), opacity : undefined,
    flipHoriz : false, flipVert : false,
    texRes : 128.0, interpolate : true, depth : -1.0 
  });
  // Initialize components for Routine "inst1"
  inst1Clock = new util.Clock();
  image_inst1 = new visual.ImageStim({
    win : psychoJS.window,
    name : 'image_inst1', units : undefined, 
    image : 'instr1.JPG', mask : undefined,
    ori : 0.0, pos : [0, 0], size : [1.7, 1],
    color : new util.Color([1, 1, 1]), opacity : undefined,
    flipHoriz : false, flipVert : false,
    texRes : 128.0, interpolate : true, depth : 0.0 
  });
  mouse_inst1 = new core.Mouse({
    win: psychoJS.window,
  });
  mouse_inst1.mouseClock = new util.Clock();
  // Initialize components for Routine "inst2"
  inst2Clock = new util.Clock();
  image_inst2 = new visual.ImageStim({
    win : psychoJS.window,
    name : 'image_inst2', units : undefined, 
    image : 'instr2.JPG', mask : undefined,
    ori : 0.0, pos : [0, 0], size : [1.7, 1],
    color : new util.Color([1, 1, 1]), opacity : undefined,
    flipHoriz : false, flipVert : false,
    texRes : 128.0, interpolate : true, depth : 0.0 
  });
  mouse_inst2 = new core.Mouse({
    win: psychoJS.window,
  });
  mouse_inst2.mouseClock = new util.Clock();
  // Initialize components for Routine "inst3"
  inst3Clock = new util.Clock();
  image_inst3 = new visual.ImageStim({
    win : psychoJS.window,
    name : 'image_inst3', units : undefined, 
    image : 'instr3.JPG', mask : undefined,
    ori : 0.0, pos : [0, 0], size : [1.7, 1],
    color : new util.Color([1, 1, 1]), opacity : undefined,
    flipHoriz : false, flipVert : false,
    texRes : 128.0, interpolate : true, depth : 0.0 
  });
  mouse_inst3 = new core.Mouse({
    win: psychoJS.window,
  });
  mouse_inst3.mouseClock = new util.Clock();
  // Initialize components for Routine "fixation"
  fixationClock = new util.Clock();
  text_fixation = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_fixation',
    text: '+',
    font: 'Open Sans',
    units: undefined, 
    pos: [0, 0], height: 0.2,  wrapWidth: undefined, ori: 0.0,
    color: new util.Color('black'),  opacity: undefined,
    depth: 0.0 
  });
  
  // Initialize components for Routine "desk"
  deskClock = new util.Clock();
  text_desk = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_desk',
    text: '책상',
    font: 'Malgun Gothic',
    units: undefined, 
    pos: [0, 0], height: 0.15,  wrapWidth: undefined, ori: 0.0,
    color: new util.Color('black'),  opacity: undefined,
    depth: 0.0 
  });
  
  mouse_desk = new core.Mouse({
    win: psychoJS.window,
  });
  mouse_desk.mouseClock = new util.Clock();
  // Initialize components for Routine "response"
  responseClock = new util.Clock();
  text_response = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_response',
    text: '60초 이내에 구체적인 기억을 입력해주세요. \n(완료되셨다면 하단의 선을 클릭해주세요. 60초가 지나면 자동으로 다음 화면으로 넘어갑니다)\n',
    font: 'Malgun Gothic',
    units: undefined, 
    pos: [0, 0.25], height: 0.03,  wrapWidth: undefined, ori: 0.0,
    color: new util.Color('black'),  opacity: undefined,
    depth: 0.0 
  });
  
  textbox = new visual.TextBox({
    win: psychoJS.window,
    name: 'textbox',
    text: ' ',
    font: 'Malgun Gothic',
    pos: [0, (- 0.1)], letterHeight: 0.03,
    size: [1.3, 0.5],  units: undefined, 
    color: 'black', colorSpace: 'rgb',
    fillColor: undefined, borderColor: 'darkslategray',
    bold: false, italic: false,
    opacity: undefined,
    padding: undefined,
    editable: true,
    multiline: true,
    anchor: 'center',
    depth: -1.0 
  });
  
  text_9 = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_9',
    text: '',
    font: 'Malgun Gothic',
    units: undefined, 
    pos: [(- 0.6), 0.3], height: 0.05,  wrapWidth: undefined, ori: 0.0,
    color: new util.Color('black'),  opacity: undefined,
    depth: -2.0 
  });
  
  slider = new visual.Slider({
    win: psychoJS.window, name: 'slider',
    size: [1.0, 0.1], pos: [0, (- 0.4)], units: 'height',
    labels: undefined, ticks: [1, 2],
    granularity: 0.0, style: ["RATING"],
    color: new util.Color('LightGray'), 
    fontFamily: 'Open Sans', bold: true, italic: false, depth: -4, 
    flip: false,
  });
  
  textbox.text = "";
  
  // Initialize components for Routine "askscreen"
  askscreenClock = new util.Clock();
  image_ask = new visual.ImageStim({
    win : psychoJS.window,
    name : 'image_ask', units : undefined, 
    image : 'ask.JPG', mask : undefined,
    ori : 0.0, pos : [0, 0], size : [1.7, 1],
    color : new util.Color([1, 1, 1]), opacity : undefined,
    flipHoriz : false, flipVert : false,
    texRes : 128.0, interpolate : true, depth : 0.0 
  });
  mouse_ask = new core.Mouse({
    win: psychoJS.window,
  });
  mouse_ask.mouseClock = new util.Clock();
  // Initialize components for Routine "fixation"
  fixationClock = new util.Clock();
  text_fixation = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_fixation',
    text: '+',
    font: 'Open Sans',
    units: undefined, 
    pos: [0, 0], height: 0.2,  wrapWidth: undefined, ori: 0.0,
    color: new util.Color('black'),  opacity: undefined,
    depth: 0.0 
  });
  
  // Initialize components for Routine "stimuli"
  stimuliClock = new util.Clock();
  text_stimuli = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_stimuli',
    text: '',
    font: 'Malgun Gothic',
    units: undefined, 
    pos: [0, 0], height: 0.15,  wrapWidth: undefined, ori: 0.0,
    color: new util.Color('black'),  opacity: undefined,
    depth: 0.0 
  });
  
  mouse_stimuli = new core.Mouse({
    win: psychoJS.window,
  });
  mouse_stimuli.mouseClock = new util.Clock();
  // Initialize components for Routine "response"
  responseClock = new util.Clock();
  text_response = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_response',
    text: '60초 이내에 구체적인 기억을 입력해주세요. \n(완료되셨다면 하단의 선을 클릭해주세요. 60초가 지나면 자동으로 다음 화면으로 넘어갑니다)\n',
    font: 'Malgun Gothic',
    units: undefined, 
    pos: [0, 0.25], height: 0.03,  wrapWidth: undefined, ori: 0.0,
    color: new util.Color('black'),  opacity: undefined,
    depth: 0.0 
  });
  
  textbox = new visual.TextBox({
    win: psychoJS.window,
    name: 'textbox',
    text: ' ',
    font: 'Malgun Gothic',
    pos: [0, (- 0.1)], letterHeight: 0.03,
    size: [1.3, 0.5],  units: undefined, 
    color: 'black', colorSpace: 'rgb',
    fillColor: undefined, borderColor: 'darkslategray',
    bold: false, italic: false,
    opacity: undefined,
    padding: undefined,
    editable: true,
    multiline: true,
    anchor: 'center',
    depth: -1.0 
  });
  
  text_9 = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_9',
    text: '',
    font: 'Malgun Gothic',
    units: undefined, 
    pos: [(- 0.6), 0.3], height: 0.05,  wrapWidth: undefined, ori: 0.0,
    color: new util.Color('black'),  opacity: undefined,
    depth: -2.0 
  });
  
  slider = new visual.Slider({
    win: psychoJS.window, name: 'slider',
    size: [1.0, 0.1], pos: [0, (- 0.4)], units: 'height',
    labels: undefined, ticks: [1, 2],
    granularity: 0.0, style: ["RATING"],
    color: new util.Color('LightGray'), 
    fontFamily: 'Open Sans', bold: true, italic: false, depth: -4, 
    flip: false,
  });
  
  textbox.text = "";
  
  // Create some handy timers
  globalClock = new util.Clock();  // to track the time since experiment started
  routineTimer = new util.CountdownTimer();  // to track time remaining of each (non-slip) routine
  
  return Scheduler.Event.NEXT;
}


var t;
var frameN;
var continueRoutine;
var gotValidClick;
var welcomeComponents;
function welcomeRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'welcome'-------
    t = 0;
    welcomeClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    // setup some python lists for storing info about the mouse_welcome
    gotValidClick = false; // until a click is received
    // keep track of which components have finished
    welcomeComponents = [];
    welcomeComponents.push(mouse_welcome);
    welcomeComponents.push(image_welcome);
    
    welcomeComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}


var prevButtonState;
var _mouseButtons;
function welcomeRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'welcome'-------
    // get current time
    t = welcomeClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    // *mouse_welcome* updates
    if (t >= 0.0 && mouse_welcome.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      mouse_welcome.tStart = t;  // (not accounting for frame time here)
      mouse_welcome.frameNStart = frameN;  // exact frame index
      
      mouse_welcome.status = PsychoJS.Status.STARTED;
      mouse_welcome.mouseClock.reset();
      prevButtonState = mouse_welcome.getPressed();  // if button is down already this ISN'T a new click
      }
    if (mouse_welcome.status === PsychoJS.Status.STARTED) {  // only update if started and not finished!
      _mouseButtons = mouse_welcome.getPressed();
      if (!_mouseButtons.every( (e,i,) => (e == prevButtonState[i]) )) { // button state changed?
        prevButtonState = _mouseButtons;
        if (_mouseButtons.reduce( (e, acc) => (e+acc) ) > 0) { // state changed to a new click
          // abort routine on response
          continueRoutine = false;
        }
      }
    }
    
    // *image_welcome* updates
    if (t >= 0.0 && image_welcome.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      image_welcome.tStart = t;  // (not accounting for frame time here)
      image_welcome.frameNStart = frameN;  // exact frame index
      
      image_welcome.setAutoDraw(true);
    }

    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    welcomeComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function welcomeRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'welcome'-------
    welcomeComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // store data for thisExp (ExperimentHandler)
    // the Routine "welcome" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}


var inst1Components;
function inst1RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'inst1'-------
    t = 0;
    inst1Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    // setup some python lists for storing info about the mouse_inst1
    gotValidClick = false; // until a click is received
    // keep track of which components have finished
    inst1Components = [];
    inst1Components.push(image_inst1);
    inst1Components.push(mouse_inst1);
    
    inst1Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}


function inst1RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'inst1'-------
    // get current time
    t = inst1Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *image_inst1* updates
    if (t >= 0.0 && image_inst1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      image_inst1.tStart = t;  // (not accounting for frame time here)
      image_inst1.frameNStart = frameN;  // exact frame index
      
      image_inst1.setAutoDraw(true);
    }

    // *mouse_inst1* updates
    if (t >= 0.0 && mouse_inst1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      mouse_inst1.tStart = t;  // (not accounting for frame time here)
      mouse_inst1.frameNStart = frameN;  // exact frame index
      
      mouse_inst1.status = PsychoJS.Status.STARTED;
      mouse_inst1.mouseClock.reset();
      prevButtonState = mouse_inst1.getPressed();  // if button is down already this ISN'T a new click
      }
    if (mouse_inst1.status === PsychoJS.Status.STARTED) {  // only update if started and not finished!
      _mouseButtons = mouse_inst1.getPressed();
      if (!_mouseButtons.every( (e,i,) => (e == prevButtonState[i]) )) { // button state changed?
        prevButtonState = _mouseButtons;
        if (_mouseButtons.reduce( (e, acc) => (e+acc) ) > 0) { // state changed to a new click
          // abort routine on response
          continueRoutine = false;
        }
      }
    }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    inst1Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


var _mouseXYs;
function inst1RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'inst1'-------
    inst1Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // store data for thisExp (ExperimentHandler)
    _mouseXYs = mouse_inst1.getPos();
    _mouseButtons = mouse_inst1.getPressed();
    psychoJS.experiment.addData('mouse_inst1.x', _mouseXYs[0]);
    psychoJS.experiment.addData('mouse_inst1.y', _mouseXYs[1]);
    psychoJS.experiment.addData('mouse_inst1.leftButton', _mouseButtons[0]);
    psychoJS.experiment.addData('mouse_inst1.midButton', _mouseButtons[1]);
    psychoJS.experiment.addData('mouse_inst1.rightButton', _mouseButtons[2]);
    // the Routine "inst1" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}


var inst2Components;
function inst2RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'inst2'-------
    t = 0;
    inst2Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    // setup some python lists for storing info about the mouse_inst2
    gotValidClick = false; // until a click is received
    // keep track of which components have finished
    inst2Components = [];
    inst2Components.push(image_inst2);
    inst2Components.push(mouse_inst2);
    
    inst2Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}


function inst2RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'inst2'-------
    // get current time
    t = inst2Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *image_inst2* updates
    if (t >= 0.0 && image_inst2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      image_inst2.tStart = t;  // (not accounting for frame time here)
      image_inst2.frameNStart = frameN;  // exact frame index
      
      image_inst2.setAutoDraw(true);
    }

    // *mouse_inst2* updates
    if (t >= 0.0 && mouse_inst2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      mouse_inst2.tStart = t;  // (not accounting for frame time here)
      mouse_inst2.frameNStart = frameN;  // exact frame index
      
      mouse_inst2.status = PsychoJS.Status.STARTED;
      mouse_inst2.mouseClock.reset();
      prevButtonState = mouse_inst2.getPressed();  // if button is down already this ISN'T a new click
      }
    if (mouse_inst2.status === PsychoJS.Status.STARTED) {  // only update if started and not finished!
      _mouseButtons = mouse_inst2.getPressed();
      if (!_mouseButtons.every( (e,i,) => (e == prevButtonState[i]) )) { // button state changed?
        prevButtonState = _mouseButtons;
        if (_mouseButtons.reduce( (e, acc) => (e+acc) ) > 0) { // state changed to a new click
          // abort routine on response
          continueRoutine = false;
        }
      }
    }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    inst2Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function inst2RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'inst2'-------
    inst2Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // store data for thisExp (ExperimentHandler)
    _mouseXYs = mouse_inst2.getPos();
    _mouseButtons = mouse_inst2.getPressed();
    psychoJS.experiment.addData('mouse_inst2.x', _mouseXYs[0]);
    psychoJS.experiment.addData('mouse_inst2.y', _mouseXYs[1]);
    psychoJS.experiment.addData('mouse_inst2.leftButton', _mouseButtons[0]);
    psychoJS.experiment.addData('mouse_inst2.midButton', _mouseButtons[1]);
    psychoJS.experiment.addData('mouse_inst2.rightButton', _mouseButtons[2]);
    // the Routine "inst2" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}


var inst3Components;
function inst3RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'inst3'-------
    t = 0;
    inst3Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    // setup some python lists for storing info about the mouse_inst3
    gotValidClick = false; // until a click is received
    // keep track of which components have finished
    inst3Components = [];
    inst3Components.push(image_inst3);
    inst3Components.push(mouse_inst3);
    
    inst3Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}


function inst3RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'inst3'-------
    // get current time
    t = inst3Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *image_inst3* updates
    if (t >= 0.0 && image_inst3.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      image_inst3.tStart = t;  // (not accounting for frame time here)
      image_inst3.frameNStart = frameN;  // exact frame index
      
      image_inst3.setAutoDraw(true);
    }

    // *mouse_inst3* updates
    if (t >= 0.0 && mouse_inst3.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      mouse_inst3.tStart = t;  // (not accounting for frame time here)
      mouse_inst3.frameNStart = frameN;  // exact frame index
      
      mouse_inst3.status = PsychoJS.Status.STARTED;
      mouse_inst3.mouseClock.reset();
      prevButtonState = mouse_inst3.getPressed();  // if button is down already this ISN'T a new click
      }
    if (mouse_inst3.status === PsychoJS.Status.STARTED) {  // only update if started and not finished!
      _mouseButtons = mouse_inst3.getPressed();
      if (!_mouseButtons.every( (e,i,) => (e == prevButtonState[i]) )) { // button state changed?
        prevButtonState = _mouseButtons;
        if (_mouseButtons.reduce( (e, acc) => (e+acc) ) > 0) { // state changed to a new click
          // abort routine on response
          continueRoutine = false;
        }
      }
    }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    inst3Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function inst3RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'inst3'-------
    inst3Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // store data for thisExp (ExperimentHandler)
    _mouseXYs = mouse_inst3.getPos();
    _mouseButtons = mouse_inst3.getPressed();
    psychoJS.experiment.addData('mouse_inst3.x', _mouseXYs[0]);
    psychoJS.experiment.addData('mouse_inst3.y', _mouseXYs[1]);
    psychoJS.experiment.addData('mouse_inst3.leftButton', _mouseButtons[0]);
    psychoJS.experiment.addData('mouse_inst3.midButton', _mouseButtons[1]);
    psychoJS.experiment.addData('mouse_inst3.rightButton', _mouseButtons[2]);
    // the Routine "inst3" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}


var fixationComponents;
function fixationRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'fixation'-------
    t = 0;
    fixationClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    routineTimer.add(1.000000);
    // update component parameters for each repeat
    // keep track of which components have finished
    fixationComponents = [];
    fixationComponents.push(text_fixation);
    
    fixationComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}


var frameRemains;
function fixationRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'fixation'-------
    // get current time
    t = fixationClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_fixation* updates
    if (t >= 0.0 && text_fixation.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_fixation.tStart = t;  // (not accounting for frame time here)
      text_fixation.frameNStart = frameN;  // exact frame index
      
      text_fixation.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_fixation.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_fixation.setAutoDraw(false);
    }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    fixationComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function fixationRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'fixation'-------
    fixationComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    return Scheduler.Event.NEXT;
  };
}


var deskComponents;
function deskRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'desk'-------
    t = 0;
    deskClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    // setup some python lists for storing info about the mouse_desk
    gotValidClick = false; // until a click is received
    // keep track of which components have finished
    deskComponents = [];
    deskComponents.push(text_desk);
    deskComponents.push(mouse_desk);
    
    deskComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}


function deskRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'desk'-------
    // get current time
    t = deskClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_desk* updates
    if (t >= 0.0 && text_desk.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_desk.tStart = t;  // (not accounting for frame time here)
      text_desk.frameNStart = frameN;  // exact frame index
      
      text_desk.setAutoDraw(true);
    }

    // *mouse_desk* updates
    if (t >= 0.0 && mouse_desk.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      mouse_desk.tStart = t;  // (not accounting for frame time here)
      mouse_desk.frameNStart = frameN;  // exact frame index
      
      mouse_desk.status = PsychoJS.Status.STARTED;
      mouse_desk.mouseClock.reset();
      prevButtonState = mouse_desk.getPressed();  // if button is down already this ISN'T a new click
      }
    if (mouse_desk.status === PsychoJS.Status.STARTED) {  // only update if started and not finished!
      _mouseButtons = mouse_desk.getPressed();
      if (!_mouseButtons.every( (e,i,) => (e == prevButtonState[i]) )) { // button state changed?
        prevButtonState = _mouseButtons;
        if (_mouseButtons.reduce( (e, acc) => (e+acc) ) > 0) { // state changed to a new click
          // abort routine on response
          continueRoutine = false;
        }
      }
    }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    deskComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function deskRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'desk'-------
    deskComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // store data for thisExp (ExperimentHandler)
    _mouseXYs = mouse_desk.getPos();
    _mouseButtons = mouse_desk.getPressed();
    psychoJS.experiment.addData('mouse_desk.x', _mouseXYs[0]);
    psychoJS.experiment.addData('mouse_desk.y', _mouseXYs[1]);
    psychoJS.experiment.addData('mouse_desk.leftButton', _mouseButtons[0]);
    psychoJS.experiment.addData('mouse_desk.midButton', _mouseButtons[1]);
    psychoJS.experiment.addData('mouse_desk.rightButton', _mouseButtons[2]);
    // the Routine "desk" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}


var responseComponents;
function responseRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'response'-------
    t = 0;
    responseClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    slider.reset()
    // keep track of which components have finished
    responseComponents = [];
    responseComponents.push(text_response);
    responseComponents.push(textbox);
    responseComponents.push(text_9);
    responseComponents.push(slider);
    
    responseComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}


function responseRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'response'-------
    // get current time
    t = responseClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_response* updates
    if (t >= 0.0 && text_response.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_response.tStart = t;  // (not accounting for frame time here)
      text_response.frameNStart = frameN;  // exact frame index
      
      text_response.setAutoDraw(true);
    }

    frameRemains = 0.0 + 60.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_response.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_response.setAutoDraw(false);
    }
    
    // *textbox* updates
    if (t >= 0.0 && textbox.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      textbox.tStart = t;  // (not accounting for frame time here)
      textbox.frameNStart = frameN;  // exact frame index
      
      textbox.setAutoDraw(true);
    }

    frameRemains = 0.0 + 60.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (textbox.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      textbox.setAutoDraw(false);
    }
    
    // *text_9* updates
    if (t >= 0.0 && text_9.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_9.tStart = t;  // (not accounting for frame time here)
      text_9.frameNStart = frameN;  // exact frame index
      
      text_9.setAutoDraw(true);
    }

    frameRemains = 0.0 + 60.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if (text_9.status === PsychoJS.Status.STARTED && t >= frameRemains) {
      text_9.setAutoDraw(false);
    }
    
    if (text_9.status === PsychoJS.Status.STARTED){ // only update if being drawn
      text_9.setText(util.round((60.0 - t)), false);
    }
    
    // *slider* updates
    if (t >= 0.0 && slider.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      slider.tStart = t;  // (not accounting for frame time here)
      slider.frameNStart = frameN;  // exact frame index
      
      slider.setAutoDraw(true);
    }

    
    // Check slider for response to end routine
    if (slider.getRating() !== undefined && slider.status === PsychoJS.Status.STARTED) {
      continueRoutine = false; }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    responseComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function responseRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'response'-------
    responseComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('textbox.text', textbox.text);
    psychoJS.experiment.addData('slider.response', slider.getRating());
    psychoJS.experiment.addData('slider.rt', slider.getRT());
    // the Routine "response" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}


var askscreenComponents;
function askscreenRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'askscreen'-------
    t = 0;
    askscreenClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    // setup some python lists for storing info about the mouse_ask
    gotValidClick = false; // until a click is received
    // keep track of which components have finished
    askscreenComponents = [];
    askscreenComponents.push(image_ask);
    askscreenComponents.push(mouse_ask);
    
    askscreenComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}


function askscreenRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'askscreen'-------
    // get current time
    t = askscreenClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *image_ask* updates
    if (t >= 0.0 && image_ask.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      image_ask.tStart = t;  // (not accounting for frame time here)
      image_ask.frameNStart = frameN;  // exact frame index
      
      image_ask.setAutoDraw(true);
    }

    // *mouse_ask* updates
    if (t >= 0.0 && mouse_ask.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      mouse_ask.tStart = t;  // (not accounting for frame time here)
      mouse_ask.frameNStart = frameN;  // exact frame index
      
      mouse_ask.status = PsychoJS.Status.STARTED;
      mouse_ask.mouseClock.reset();
      prevButtonState = mouse_ask.getPressed();  // if button is down already this ISN'T a new click
      }
    if (mouse_ask.status === PsychoJS.Status.STARTED) {  // only update if started and not finished!
      _mouseButtons = mouse_ask.getPressed();
      if (!_mouseButtons.every( (e,i,) => (e == prevButtonState[i]) )) { // button state changed?
        prevButtonState = _mouseButtons;
        if (_mouseButtons.reduce( (e, acc) => (e+acc) ) > 0) { // state changed to a new click
          // abort routine on response
          continueRoutine = false;
        }
      }
    }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    askscreenComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function askscreenRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'askscreen'-------
    askscreenComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // store data for thisExp (ExperimentHandler)
    _mouseXYs = mouse_ask.getPos();
    _mouseButtons = mouse_ask.getPressed();
    psychoJS.experiment.addData('mouse_ask.x', _mouseXYs[0]);
    psychoJS.experiment.addData('mouse_ask.y', _mouseXYs[1]);
    psychoJS.experiment.addData('mouse_ask.leftButton', _mouseButtons[0]);
    psychoJS.experiment.addData('mouse_ask.midButton', _mouseButtons[1]);
    psychoJS.experiment.addData('mouse_ask.rightButton', _mouseButtons[2]);
    // the Routine "askscreen" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}


var trials;
var currentLoop;
function trialsLoopBegin(trialsLoopScheduler) {
  // set up handler to look after randomisation of conditions etc
  trials = new TrialHandler({
    psychoJS: psychoJS,
    nReps: 1, method: TrialHandler.Method.RANDOM,
    extraInfo: expInfo, originPath: undefined,
    trialList: 'worditem.xlsx',
    seed: undefined, name: 'trials'
  });
  psychoJS.experiment.addLoop(trials); // add the loop to the experiment
  currentLoop = trials;  // we're now the current loop

  // Schedule all the trials in the trialList:
  trials.forEach(function() {
    const snapshot = trials.getSnapshot();

    trialsLoopScheduler.add(importConditions(snapshot));
    trialsLoopScheduler.add(fixationRoutineBegin(snapshot));
    trialsLoopScheduler.add(fixationRoutineEachFrame(snapshot));
    trialsLoopScheduler.add(fixationRoutineEnd(snapshot));
    trialsLoopScheduler.add(stimuliRoutineBegin(snapshot));
    trialsLoopScheduler.add(stimuliRoutineEachFrame(snapshot));
    trialsLoopScheduler.add(stimuliRoutineEnd(snapshot));
    trialsLoopScheduler.add(responseRoutineBegin(snapshot));
    trialsLoopScheduler.add(responseRoutineEachFrame(snapshot));
    trialsLoopScheduler.add(responseRoutineEnd(snapshot));
    trialsLoopScheduler.add(endLoopIteration(trialsLoopScheduler, snapshot));
  });

  return Scheduler.Event.NEXT;
}


function trialsLoopEnd() {
  psychoJS.experiment.removeLoop(trials);

  return Scheduler.Event.NEXT;
}


var stimuliComponents;
function stimuliRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'stimuli'-------
    t = 0;
    stimuliClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    text_stimuli.setText(worditem);
    // setup some python lists for storing info about the mouse_stimuli
    gotValidClick = false; // until a click is received
    // keep track of which components have finished
    stimuliComponents = [];
    stimuliComponents.push(text_stimuli);
    stimuliComponents.push(mouse_stimuli);
    
    stimuliComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}


function stimuliRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'stimuli'-------
    // get current time
    t = stimuliClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *text_stimuli* updates
    if (t >= 0.0 && text_stimuli.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_stimuli.tStart = t;  // (not accounting for frame time here)
      text_stimuli.frameNStart = frameN;  // exact frame index
      
      text_stimuli.setAutoDraw(true);
    }

    // *mouse_stimuli* updates
    if (t >= 0.0 && mouse_stimuli.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      mouse_stimuli.tStart = t;  // (not accounting for frame time here)
      mouse_stimuli.frameNStart = frameN;  // exact frame index
      
      mouse_stimuli.status = PsychoJS.Status.STARTED;
      mouse_stimuli.mouseClock.reset();
      prevButtonState = mouse_stimuli.getPressed();  // if button is down already this ISN'T a new click
      }
    if (mouse_stimuli.status === PsychoJS.Status.STARTED) {  // only update if started and not finished!
      _mouseButtons = mouse_stimuli.getPressed();
      if (!_mouseButtons.every( (e,i,) => (e == prevButtonState[i]) )) { // button state changed?
        prevButtonState = _mouseButtons;
        if (_mouseButtons.reduce( (e, acc) => (e+acc) ) > 0) { // state changed to a new click
          // abort routine on response
          continueRoutine = false;
        }
      }
    }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    stimuliComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}


function stimuliRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'stimuli'-------
    stimuliComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // store data for thisExp (ExperimentHandler)
    _mouseXYs = mouse_stimuli.getPos();
    _mouseButtons = mouse_stimuli.getPressed();
    psychoJS.experiment.addData('mouse_stimuli.x', _mouseXYs[0]);
    psychoJS.experiment.addData('mouse_stimuli.y', _mouseXYs[1]);
    psychoJS.experiment.addData('mouse_stimuli.leftButton', _mouseButtons[0]);
    psychoJS.experiment.addData('mouse_stimuli.midButton', _mouseButtons[1]);
    psychoJS.experiment.addData('mouse_stimuli.rightButton', _mouseButtons[2]);
    // the Routine "stimuli" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}


function endLoopIteration(scheduler, snapshot) {
  // ------Prepare for next entry------
  return function () {
    if (typeof snapshot !== 'undefined') {
      // ------Check if user ended loop early------
      if (snapshot.finished) {
        // Check for and save orphaned data
        if (psychoJS.experiment.isEntryEmpty()) {
          psychoJS.experiment.nextEntry(snapshot);
        }
        scheduler.stop();
      } else {
        const thisTrial = snapshot.getCurrentTrial();
        if (typeof thisTrial === 'undefined' || !('isTrials' in thisTrial) || thisTrial.isTrials) {
          psychoJS.experiment.nextEntry(snapshot);
        }
      }
    return Scheduler.Event.NEXT;
    }
  };
}


function importConditions(currentLoop) {
  return function () {
    psychoJS.importAttributes(currentLoop.getCurrentTrial());
    return Scheduler.Event.NEXT;
    };
}


function quitPsychoJS(message, isCompleted) {
  // Check for and save orphaned data
  if (psychoJS.experiment.isEntryEmpty()) {
    psychoJS.experiment.nextEntry();
  }
  
  
  psychoJS.window.close();
  psychoJS.quit({message: message, isCompleted: isCompleted});
  
  return Scheduler.Event.QUIT;
}
